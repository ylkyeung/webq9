// JavaScript Document
var q9Data;
var q9CHSData;

var q9AssociateData;
var q9CHSAssociateData;

var cj5;
var cj5CHS;

var cangjie;
var cangjieCHS;

var cantonhk;
/*
$(document).keypress(function(event){
	console.log(event);
	alert(String.fromCharCode(event.which));
});
*/

$( document ).ready(function() {
    init();

	$("body").on("keydown",function(e) {
		var key = keyMap(e.keyCode);
        var keyChar = String.fromCharCode(e.keyCode);
        return keydown_action(key);
	});
});

function init() {
    //init_data();
    init_button();
}

function init_data(){
    $.getJSON( "data/Q9.json", function(json){
        q9Data = json;
    });
    $.getJSON( "data/Q9_CHS.json", function(json){
        q9CHSData = json;
    });

    $.getJSON( "data/Q9_Associate.json", function(json){
        q9AssociateData = json;
    });
    $.getJSON( "data/Q9_CHS_Associate.json", function(json){
        q9CHSAssociateData = json;
    });

    $.getJSON( "data/cangjie.json", function(json){
        cangjie =json;
    });

    $.getJSON( "data/cj5.json", function(json){
        cj5 =json;
    });

    $.getJSON( "data/cj5_CHS.json", function(json){
        cj5CHS =json;
    });

    $.getJSON( "data/cantonhk.json", function(json){
        cantonhk =json;
    });
}

function init_button(){
    $(".q9_num_dot").on("click",function(e) {
        keydown_action(110);
    });
    $(".q9_num0").on("click",function(e) {
        keydown_action(96);
    });
    $(".q9_num1").on("click",function(e) {
        keydown_action(97);
    });
    $(".q9_num2").on("click",function(e) {
        keydown_action(98);
    });
    $(".q9_num3").on("click",function(e) {
        keydown_action(99);
    });
    $(".q9_num4").on("click",function(e) {
        keydown_action(100);
    });
    $(".q9_num5").on("click",function(e) {
        keydown_action(101);
    });
    $(".q9_num6").on("click",function(e) {
        keydown_action(102);
    });
    $(".q9_num7").on("click",function(e) {
        keydown_action(103);
    });
    $(".q9_num8").on("click",function(e) {
        keydown_action(104);
    });
    $(".q9_num9").on("click",function(e) {
        keydown_action(105);
    });
}

function keydown_action(key){
    //console.log(key + ": " + keyChar);
    // If the user has pressed enter

    if(key==107){
        insteadCantonhkInput();
        return false;
    }else if(key==110){
        reset_all();
        return false;
    }else if(key>=96 && key<=105) {
        if($(".q9_code").val().substr(0,1)!=0 && $(".q9_code").val().length==1 && key==96 ){
            return false;
        }
        var q9_codeValue = $(".q9_code").val();
        $(".q9_code").val(q9_codeValue +""+ (key-96));
        find_word_list();
        return false;
    }else if(key==109){
        var q9_codeValue = $(".q9_code").val();
        var index = q9_codeValue.lastIndexOf("00");
        if(index>=0){
            $(".q9_code").val(replace_last_double_zero(q9_codeValue));
        }else if( q9_codeValue.lastIndexOf("0")>=0 && q9_codeValue.lastIndexOf("0") == q9_codeValue.length-1 && q9_codeValue.length>3){
            $(".q9_code").val(replace_last_zero(q9_codeValue));
        }
        find_word_list();
        return false;
    }else{
        return true;
    }
}

function keyMap(inputKey){

	if($(".q9_nb_input").is(':checked')){

		if(inputKey==55){
			return 103;
		}else if(inputKey==56){
			return 104;
		}else if(inputKey==57){
			return 105;

		}else if(inputKey==85){
			return 100;
		}else if(inputKey==73){
			return 101;
		}else if(inputKey==79){
			return 102;

		}else if(inputKey==74){
			return 97;
		}else if(inputKey==75){
			return 98;
		}else if(inputKey==76){
			return 99;

		}else if(inputKey==77){
			return 96;
		}else if(inputKey==188){
			return 110;
		}else {
			return inputKey;
		}
	}
	return inputKey;
}

function replace_last_double_zero(text) {
	var index = text.lastIndexOf('00');
	return text.substring(0, index) + '0';
}

function replace_last_zero(text) {
	var index = text.lastIndexOf('0');
	return text.substring(0, index);
}

function reset_all(){
	reset_q9_code();
	reset_num();
}

function reset_q9_code(){
	$(".q9_code").val("");
}

function reset_num(){
	$(".q9_num1").html('&nbsp;');
	$(".q9_num2").html('&nbsp;');
	$(".q9_num3").html('&nbsp;');
	$(".q9_num4").html('&nbsp;');
	$(".q9_num5").html('&nbsp;');
	$(".q9_num6").html('&nbsp;');
	$(".q9_num7").html('&nbsp;');
	$(".q9_num8").html('&nbsp;');
	$(".q9_num9").html('&nbsp;');
	$(".q9_img_body").attr('src',"image/q9_s.png");
}

function isCHSInputOn(){
	if($(".q9_chs").is(':checked')){
		return true;
	}
	return false;
}
function isCantonhkInputOn(){
	if($(".q9_cantonhk").is(':checked')){
		return true;
	}
	return false;
}

function insteadCantonhkInput(){
	if(isCantonhkInputOn()){
		$( ".q9_cantonhk" ).prop( "checked", false );
	}else{
		$( ".q9_cantonhk" ).prop( "checked", true );
	}

}


(function ($, undefined) {
	$.fn.getCursorPosition = function() {
		var el = $(this).get(0);
		var pos = 0;
		if('selectionStart' in el) {
			pos = el.selectionStart;
		} else if('selection' in document) {
			el.focus();
			var Sel = document.selection.createRange();
			var SelLength = document.selection.createRange().text.length;
			Sel.moveStart('character', -el.value.length);
			pos = Sel.text.length - SelLength;
		}
		return pos;
	}
})(jQuery);

$.fn.selectRange = function(start, end) {
	if(end === undefined) {
		end = start;
	}
	return this.each(function() {
		if('selectionStart' in this) {
			this.selectionStart = start;
			this.selectionEnd = end;
		} else if(this.setSelectionRange) {
			this.setSelectionRange(start, end);
		} else if(this.createTextRange) {
			var range = this.createTextRange();
			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();
		}
	});
};

function find_word_list(){
	$(".q9_code").removeClass("max");
	$(".q9_munber_pad").removeClass("select");
	$(".q9_img_body").attr('src',"image/q9_e.png");

	var q9_codeValue = $(".q9_code").val();

	var lastWord = q9_codeValue.substring(q9_codeValue.length-1);
	if(!isCantonhkInputOn()){
		if(lastWord == "]"){
			$(".q9_munber_pad").addClass("select");
			$(".q9_img_body").attr('src',"image/q9_c.png");
		}
	}


	var index = q9_codeValue.lastIndexOf("[");
	if(index>=0){
		if(!isCantonhkInputOn()){
			find_accessories_word_list_by_word( q9_codeValue.substring(index));
		}else{
			find_cantonhk_word_list_by_word(q9_codeValue.substring(index));
		}
	}else{
		find_word_list_by_word(q9_codeValue);
	}
}

function find_word_list_by_word(keyword){
	var prefixes='';
	var suffixes='';

	var needFindWord = false;

	var index = keyword.indexOf('0');
	if (index>=0 && index<3){
		prefixes = keyword.substring(0, index+1);
		suffixes = keyword.substring(index+1);
		needFindWord = true;
	}else if(keyword.length >=3){
		prefixes = keyword.substring(0, 3);
		suffixes = keyword.substring(3);
		needFindWord = true;
	}else{
		prefixes = keyword;
		suffixes = '';
		needFindWord = false;
	}

	//console.log("prefixes: "+prefixes);
	if(prefixes.length==1 && prefixes.substring(0, 1)!=0){
		if(prefixes>=1 && prefixes<=9){
			$(".q9_img_body").attr('src',"image/q9_"+prefixes+".png");
		}
	}else if(prefixes.length==2 && prefixes.substring(0, 1)!=0){
		$(".q9_img_body").attr('src',"image/q9_n.png");
	}else{
		$(".q9_img_body").attr('src',"image/q9_e.png");
	}

	if(needFindWord){
		var wordPage = suffixes.lastIndexOf("0")+1;
		var suffixesLastWord = suffixes.substring(suffixes.length-1);

		//console.log((wordPage+1)*9);
		//console.log(q9Data[prefixes].length);

		if( typeof q9Data[prefixes] == 'undefined' || (wordPage+1)*(9) > q9Data[prefixes].length){
			suffixes = suffixes.substring(0,suffixes.lastIndexOf("0"));
			$(".q9_code").val(prefixes+suffixes);
			wordPage = suffixes.lastIndexOf("0")+1;
			suffixesLastWord = suffixes.substring(suffixes.length-1);
			$(".q9_code").addClass("max");
			return;
		}

		if(suffixesLastWord == 0 || suffixesLastWord==''){
			var wordIndex = wordPage * 9;
			$(".q9_num7").html(q9Data[prefixes][wordIndex+0]);
			$(".q9_num8").html(q9Data[prefixes][wordIndex+1]);
			$(".q9_num9").html(q9Data[prefixes][wordIndex+2]);
			$(".q9_num4").html(q9Data[prefixes][wordIndex+3]);
			$(".q9_num5").html(q9Data[prefixes][wordIndex+4]);
			$(".q9_num6").html(q9Data[prefixes][wordIndex+5]);
			$(".q9_num1").html(q9Data[prefixes][wordIndex+6]);
			$(".q9_num2").html(q9Data[prefixes][wordIndex+7]);
			$(".q9_num3").html(q9Data[prefixes][wordIndex+8]);

		}else{
			var fixedSuffixesLastWord = suffixesLastWord;
			switch(parseInt(suffixesLastWord)) {
				case 1:
					fixedSuffixesLastWord = 7;
					break;
				case 2:
					fixedSuffixesLastWord = 8;
					break;
				case 3:
					fixedSuffixesLastWord = 9;
					break;
				case 7:
					fixedSuffixesLastWord = 1;
					break;
				case 8:
					fixedSuffixesLastWord = 2;
					break;
				case 9:
					fixedSuffixesLastWord = 3;
					break;
				default:
					fixedSuffixesLastWord =parseInt(suffixesLastWord);
			}


			var wordIndex = wordPage * 9 + fixedSuffixesLastWord-1;

			//console.log("wordIndex: "+wordIndex);
			//console.log("q9Data[prefixes].length: "+q9Data[prefixes].length);
			//console.log("word: "+q9Data[prefixes][wordIndex]);
			var output ="";
			var output_CHT ="";
			var output_CHS ="";
			if(wordIndex <= q9Data[prefixes].length){
				var cursorPos = $(".inputTextArea").getCursorPosition();
				//console.log(cursorPos);
				var inputTextAreaPrefixes = $(".inputTextArea").val().substring(0,cursorPos);
				var inputTextAreaSuffixes = $(".inputTextArea").val().substring(cursorPos);
				//console.log(inputTextAreaPrefixes);
				//console.log(inputTextAreaSuffixes);
				//console.log("");
				output_CHT =  q9Data[prefixes][wordIndex];
				output_CHS = q9CHSData[prefixes][wordIndex];
				output = output_CHT;
				if(isCHSInputOn()){
					output =output_CHS
				}
				if(!isCantonhkInputOn()){
					$(".inputTextArea").val(inputTextAreaPrefixes+output+inputTextAreaSuffixes);
					$(".inputTextArea").selectRange(cursorPos+1);
				}
			}
			var q9_codeValue = $(".q9_code").val();
			$(".q9_code").val(q9_codeValue+"["+output+"]");
			display_info(output);
			find_word_list();
			//reset_all();
		}
	}

	//console.log(prefixes + " | " + suffixes);
	//console.log(q9Data[prefixes]);
}

function find_accessories_word_list_by_word(code){

	var prefixes="";
	var suffixes="";
	var preIndex= code.lastIndexOf("[");
	var sufIndex = code.lastIndexOf("]");
	//console.log("preIndex: "+ preIndex+"; sufIndex: "+sufIndex);

	if(preIndex < sufIndex &&  preIndex>-1 && sufIndex>-1 && sufIndex - preIndex  > 1){
		prefixes = code.substring(sufIndex-1, sufIndex);
	}else{
		prefixes=' ';
	}
	if(preIndex < sufIndex && sufIndex>-1){
		suffixes = code.substring(sufIndex+1);
	}
	//console.log("prefixes: "+ prefixes+"; suffixes: "+suffixes);

	var suffixesFirstWord = suffixes.substring(0,1);
	//console.log("suffixesFirstWord: "+ suffixesFirstWord);
	if(suffixesFirstWord>=1 && suffixesFirstWord<=9){
		reset_all();
		$(".q9_code").val(suffixesFirstWord);
		find_word_list();
		return;
	}

	var associateDataList;
	if(isCHSInputOn()) {
		associateDataList = q9CHSAssociateData[prefixes];
	}else{
		associateDataList = q9AssociateData[prefixes];
	}
	if(typeof associateDataList == 'undefined'){
		if(isCHSInputOn()) {
			associateDataList = q9CHSAssociateData[" "];
		}else{
			associateDataList = q9AssociateData[" "];
		}
	}
	//console.log(associateDataList);

	var wordPage = suffixes.lastIndexOf("0");
	if(wordPage<0){
		wordPage=0;
	}

	var suffixesLastWord = suffixes.substring(suffixes.length-1);

	//console.log(wordPage);
	//console.log("suffixesLastWord: "+suffixesLastWord);
	//console.log(associateDataList.length);

	if( typeof associateDataList == 'undefined' || (wordPage)*(9) >= associateDataList.length){
		var q9_codeValue = $(".q9_code").val();
		$(".q9_code").val(q9_codeValue.substring(0, q9_codeValue.length-1));
		$(".q9_code").addClass("max");
		//console.log("max");
		return;
	}

	if(suffixesLastWord == 0 || suffixesLastWord==''){
		var wordIndex = wordPage * 9;
		$(".q9_num1").html( typeof associateDataList[wordIndex+0] == 'undefined' ? "" : associateDataList[wordIndex+0] );
		$(".q9_num2").html( typeof associateDataList[wordIndex+1] == 'undefined' ? "" : associateDataList[wordIndex+1] );
		$(".q9_num3").html( typeof associateDataList[wordIndex+2] == 'undefined' ? "" : associateDataList[wordIndex+2] );
		$(".q9_num4").html( typeof associateDataList[wordIndex+3] == 'undefined' ? "" : associateDataList[wordIndex+3] );
		$(".q9_num5").html( typeof associateDataList[wordIndex+4] == 'undefined' ? "" : associateDataList[wordIndex+4] );
		$(".q9_num6").html( typeof associateDataList[wordIndex+5] == 'undefined' ? "" : associateDataList[wordIndex+5] );
		$(".q9_num7").html( typeof associateDataList[wordIndex+6] == 'undefined' ? "" : associateDataList[wordIndex+6] );
		$(".q9_num8").html( typeof associateDataList[wordIndex+7] == 'undefined' ? "" : associateDataList[wordIndex+7] );
		$(".q9_num9").html( typeof associateDataList[wordIndex+8] == 'undefined' ? "" : associateDataList[wordIndex+8] );

	}else{
		var fixedSuffixesLastWord = suffixesLastWord;
		var wordIndex = wordPage * 9 + fixedSuffixesLastWord-1;


		var output ="";
		var output_CHT ="";
		var output_CHS ="";
		if(wordIndex <= associateDataList.length){
			var cursorPos = $(".inputTextArea").getCursorPosition();
			//console.log(cursorPos);
			var inputTextAreaPrefixes = $(".inputTextArea").val().substring(0,cursorPos);
			var inputTextAreaSuffixes = $(".inputTextArea").val().substring(cursorPos);
			//console.log(inputTextAreaPrefixes);
			//console.log(inputTextAreaSuffixes);
			//console.log("");
			output_CHT = associateDataList[wordIndex];
			output_CHS = associateDataList[wordIndex];
			output = output_CHT;
			if(isCHSInputOn()){
				output =output_CHS
			}
			$(".inputTextArea").val(inputTextAreaPrefixes+output+inputTextAreaSuffixes);
			$(".inputTextArea").selectRange(cursorPos+1);
		}
		var q9_codeValue = $(".q9_code").val();
		$(".q9_code").val(q9_codeValue+"["+output+"]");
		display_info(output);

		find_word_list();
	}


	//console.log(prefixes + " | " + suffixes);
	//console.log(q9Data[prefixes]);
}

function find_cantonhk_word_list_by_word(code){

	var prefixes="";
	var suffixes="";
	var preIndex= code.lastIndexOf("[");
	var sufIndex = code.lastIndexOf("]");
	//console.log("preIndex: "+ preIndex+"; sufIndex: "+sufIndex);

	if(preIndex < sufIndex &&  preIndex>-1 && sufIndex>-1 && sufIndex - preIndex  > 1){
		prefixes = code.substring(sufIndex-1, sufIndex);
	}else{
		prefixes=' ';
	}
	if(preIndex < sufIndex && sufIndex>-1){
		suffixes = code.substring(sufIndex+1);
	}
	//console.log("prefixes: "+ prefixes+"; suffixes: "+suffixes);

	var suffixesFirstWord = suffixes.substring(0,1);
	//console.log("suffixesFirstWord: "+ suffixesFirstWord);

	var associateDataList = find_cantonhk_list(prefixes);
	//e.log(associateDataList);

	var wordPage = suffixes.lastIndexOf("0")+1;
	if(wordPage<0){
		wordPage=0;
	}

	var suffixesLastWord = suffixes.substring(suffixes.length-1);

	//console.log("wordPage: "+wordPage);
	//console.log("suffixesLastWord: "+suffixesLastWord);
	//console.log(associateDataList.length);

	if( typeof associateDataList == 'undefined' || (wordPage)*(9) >= associateDataList.length){
		var q9_codeValue = $(".q9_code").val();
		$(".q9_code").val(q9_codeValue.substring(0, q9_codeValue.length-1));
		$(".q9_code").addClass("max");
		//console.log("max");
		return;
	}

	if(suffixesLastWord == 0 || suffixesLastWord==''){
		var wordIndex = wordPage * 9;
		$(".q9_num1").html( typeof associateDataList[wordIndex+0] == 'undefined' ? "" : associateDataList[wordIndex+0] );
		$(".q9_num2").html( typeof associateDataList[wordIndex+1] == 'undefined' ? "" : associateDataList[wordIndex+1] );
		$(".q9_num3").html( typeof associateDataList[wordIndex+2] == 'undefined' ? "" : associateDataList[wordIndex+2] );
		$(".q9_num4").html( typeof associateDataList[wordIndex+3] == 'undefined' ? "" : associateDataList[wordIndex+3] );
		$(".q9_num5").html( typeof associateDataList[wordIndex+4] == 'undefined' ? "" : associateDataList[wordIndex+4] );
		$(".q9_num6").html( typeof associateDataList[wordIndex+5] == 'undefined' ? "" : associateDataList[wordIndex+5] );
		$(".q9_num7").html( typeof associateDataList[wordIndex+6] == 'undefined' ? "" : associateDataList[wordIndex+6] );
		$(".q9_num8").html( typeof associateDataList[wordIndex+7] == 'undefined' ? "" : associateDataList[wordIndex+7] );
		$(".q9_num9").html( typeof associateDataList[wordIndex+8] == 'undefined' ? "" : associateDataList[wordIndex+8] );

	}else{
		var fixedSuffixesLastWord = suffixesLastWord;
		var wordIndex = wordPage * 9 + parseInt(fixedSuffixesLastWord)-1;
		//console.log(wordIndex);

		var output ="";
		var output_CHT ="";
		var output_CHS ="";

		if(wordIndex <= associateDataList.length){
			var cursorPos = $(".inputTextArea").getCursorPosition();
			//console.log(cursorPos);
			var inputTextAreaPrefixes = $(".inputTextArea").val().substring(0,cursorPos);
			var inputTextAreaSuffixes = $(".inputTextArea").val().substring(cursorPos);
			//console.log(inputTextAreaPrefixes);
			//console.log(inputTextAreaSuffixes);
			//console.log("");
			output_CHT = associateDataList[wordIndex];
			output_CHS = associateDataList[wordIndex];
			output = output_CHT;
			if(isCHSInputOn()){
				output =output_CHS
			}
			$(".inputTextArea").val(inputTextAreaPrefixes+output+inputTextAreaSuffixes);
			$(".inputTextArea").selectRange(cursorPos+1);
		}
		var q9_codeValue = $(".q9_code").val();
		$(".q9_code").val(q9_codeValue+"["+output+"]");
		display_info(output);
		$(".q9_cantonhk").prop( "checked", false );
		find_word_list();
	}


	//console.log(prefixes + " | " + suffixes);
	//console.log(q9Data[prefixes]);
}

function display_info(key){
	var result=false;
	if(!isCHSInputOn()) {
		result = find_info(key, cj5);
	}else{
		result = find_info(key, cj5CHS);
	}
	if(result!=false){
		var display_string = key+": "+result;
		$(".cj5_display").val(display_string);
		$(".q9_info_cj5_display").html("倉頡: "+result);
	}

}

function find_info(key, data){
	var  display_string="";
	var key_eng = data.chardef[key];
	if(typeof key_eng == 'undefined'){
		return false;
	}
	for(var i=0; i<key_eng.length;i++){
		var s = key_eng.substring(i,i+1);
		//console.log("s:"+s);
		var keyName = data.keyname[s];
		//console.log("keyName:"+keyName);
		if(typeof keyName == 'undefined'){
			return false;
		}
		display_string+=keyName
	}
	//display_string = key+": "+display_string+"("+key_eng.toUpperCase()+")";
	display_string = display_string+"("+key_eng.toUpperCase()+")";
	console.log(key+": "+display_string);
	return display_string;
}

function find_cantonhk_list(key){
	var return_list = [];
	var return_map = {};
	return_map[key] = 100;
	var cantonhk_sounds = cantonhk.word[key];
	//console.log(cantonhk_sounds);
	if(typeof cantonhk_sounds != 'undefined'){
		if(!Array.isArray(cantonhk_sounds)){
			var s = cantonhk_sounds;
			cantonhk_sounds=[];
			cantonhk_sounds.push(s);
		}

		for(var i =0; i<cantonhk_sounds.length;i++){
			//console.log(cantonhk_sounds[i]);
			var cantonhk_words = cantonhk.sound[cantonhk_sounds[i]];
			//console.log(Array.isArray(cantonhk_words));
			if(typeof cantonhk_words != "undefined"){
				if(!Array.isArray(cantonhk_words)){
					var s = cantonhk_words;
					cantonhk_words=[];
					cantonhk_words.push(s);
				}
				//console.log(cantonhk_words);
				for(var x=0; x< cantonhk_words.length; x++){
					//console.log(cantonhk_words[x]);
					var word = cantonhk_words[x];
					/*if(!return_list.includes(word)){
						return_list.push(word);
					}*/
					if(typeof return_map[word] == "undefined"){
						return_map[word] = 1;
					}else{
						return_map[word] =return_map[word]+1;
					}
				}
			}
		}
	}

	var array_list = [];
	for (var key in return_map) {
		array_list.push({
			name: key,
			value: return_map[key]
		});
	}
	var array_list = array_list.sort(function(a, b) {
		return (a.value < b.value) ? 1 : ((b.value < a.value) ? -1 : 0)
	});
	//console.log(array_list);
	for(var i =0; i< array_list.length;i++){
		return_list.push(array_list[i]["name"]);
	}
	return return_list;
}

/*
function findCJ5Code(key) {
	if (isCHSInputOn()) {
		return val2key(key, cj5CHS.chardefs);
	} else {
		return val2key(key, cj5.chardefs);
	}
}
function findCJ5KeyName(key) {
	if (isCHSInputOn()) {
		return cj5CHS.keynames[key.toLowerCase()];
	} else {
		return cj5.keynames[key.toLowerCase()];
	}
}
function val2key(val,array){
	for (var key in array) {
		//console.log(array[key]);
		for(var i =0;  i < array[key].length ;i++){
			//console.log(array[key][i]);
			if(array[key][i]==val){
				return key.toUpperCase();
			}
		}
	}
	return false;
}
*/